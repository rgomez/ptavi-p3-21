import sys
from xml.sax import make_parser
from smil import SMILHandler
import json


def to_string(list):
    solve = ""
    for element in list:
        solve = solve + element['name']
        for key, value in element['attrs']:
            solve = solve + f'\t{key}="{value}"'
        solve = solve + '\n'
    return solve


def to_json(list, file):
    with open(file.split(".")[0] + ".json", 'w') as ficherojson:
        json.dump(list, ficherojson, indent=2)


def main():
    if len(sys.argv) == 2:
        try:
            file = sys.argv[1]
            json = False
        except IndexError:
            sys.exit('Usage: python3 karaoke.py <file>')
    else:
        if sys.argv[1] == "--json":
            file = sys.argv[2]
            json = True
        else:
            sys.exit('Usage: python3 karaoke.py --json <file>')

    parser = make_parser()
    kHandler = SMILHandler()
    parser.setContentHandler(kHandler)
    parser.parse(open(file))
    list = kHandler.get_tags()
    if json == False:
        print(to_string(list), end="")
    else:
        print(to_json(list, file), end="")


if __name__ == '__main__':
    main()
