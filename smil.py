﻿from xml.sax.handler import ContentHandler
from xml.sax import make_parser


class SMILHandler(ContentHandler):

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """
        self.list = []
        self.rootlayout = {}
        self.region = {}
        self.img = {}
        self.audio = {}

        self.textstream = {}

        # Diccionario por cada uno
        self.diccionario = {}
        self.diccionario2 = {}
        self.diccionario3 = {}
        self.diccionario4 = {}
        self.diccionario5 = {}

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """
        if name == 'root-layout':
            # De esta manera tomamos los valores de los atributos
            self.width = attrs.get('width', "")
            self.height = attrs.get('height', "")
            self.bgcolor = attrs.get('background-color', "")
            self.rootlayout = {'width': self.width, 'height': self.height, 'background-color': self.bgcolor}
            # Añadimos claves y valores al diccionario
            pairs = self.rootlayout.items()  # Devuelve una lista de tuplas
            listpairs = list(pairs)  # Hace una lista de pairs
            self.diccionario = {'attrs': listpairs,
                                'name': name}
            # Añade las claves attrs y name con sus respectivos valores

            self.rootlayout = {}

        elif name == 'region':

            self.id = attrs.get('id', "")
            self.top = attrs.get('top', "")
            self.bottom = attrs.get('bottom', "")
            self.left = attrs.get('left', "")
            self.right = attrs.get('right', "")
            self.left = attrs.get('left', "")
            self.region = {'id': self.id, 'top': self.top, 'bottom': self.bottom, 'left': self.left,
                           'right': self.right}
            # Añadimos claves y valores al diccionario
            pairs2 = self.region.items()  # Devuelve una lista de tuplas
            listpairs2 = list(pairs2)  # Hace una lista de pairs
            self.diccionario2 = {'attrs': listpairs2,
                                 'name': name}
            # Añade las claves attrs y name con sus respectivos valores
            self.region = {}
        elif name == 'img':
            self.src = attrs.get('src', "")
            self.region = attrs.get('region', "")
            self.begin = attrs.get('begin', "")
            self.end = attrs.get('end', "")
            self.dur = attrs.get('dur', "")
            self.img = {'src': self.src, 'region': self.region, 'begin': self.begin, 'end': self.end,
                        'dur': self.dur}
            # Añadimos claves y valores al diccionario
            pairs3 = self.img.items()  # Devuelve una lista de tuplas
            listpairs3 = list(pairs3)  # Hace una lista de pairs
            self.diccionario3 = {'attrs': listpairs3,
                                 'name': name}
            # Añade las claves attrs y name con sus respectivos valores

            self.img = {}

        elif name == 'audio':
            self.src = attrs.get('src', "")
            self.begin = attrs.get('begin', "")
            self.dur = attrs.get('dur', "")
            self.audio = {'src': self.src, 'begin': self.begin, 'dur': self.dur}
            # Añadimos claves y valores al diccionario
            pairs4 = self.audio.items()  # Devuelve una lista de tuplas
            listpairs4 = list(pairs4)  # Hace una lista de pairs
            self.diccionario4 = {'attrs': listpairs4,
                                 'name': name}
            # Añade las claves attrs y name con sus respectivos valores

            self.audio = {}

        elif name == 'textstream':
            self.src = attrs.get('src', "")
            self.region = attrs.get('region', "")
            self.fill = attrs.get('fill', "")
            self.textstream = {'src': self.src, 'region': self.region, 'fill': self.fill}
            # Añadimos claves y valores al diccionario
            pairs5 = self.textstream.items()  # Devuelve una lista de tuplas
            listpairs5 = list(pairs5)  # Hace una lista de pairs
            self.diccionario5 = {'attrs': listpairs5,
                                 'name': name}
            # Añade las claves attrs y name con sus respectivos valores

            self.textstream = {}

    def get_tags(self):
        self.list.append(self.diccionario)
        self.list.append(self.diccionario2)
        self.list.append(self.diccionario3)
        self.list.append(self.diccionario4)
        self.list.append(self.diccionario5)
        return self.list


def main():
    """Programa principal"""
    parser = make_parser()
    sHandler = SMILHandler()
    parser.setContentHandler(sHandler)
    parser.parse(open('karaoke.smil'))
    print(sHandler.get_tags())


if __name__ == "__main__":
    main()

